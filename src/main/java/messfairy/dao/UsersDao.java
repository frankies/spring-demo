package messfairy.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import messfairy.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class UsersDao{
    private JdbcTemplate template;
    private NamedParameterJdbcTemplate jdbcTemplate;
    private RowMapper userMapper = new RowMapper() {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
            User user = new User();
            user.setId(Integer.valueOf(rs.getInt("id")));
            user.setName(rs.getString("name"));
            user.setPassword(rs.getString("password"));
            user.setCreateDate(rs.getDate("create_date"));
            return user;
        }
    };
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.template = new JdbcTemplate(dataSource);
    }
    public void create(User user) {
        String insertSql = "INSERT INTO USERS(name,  first_name,  last_name,  password,  create_date) VALUES (?, ?, ?, ?, ?)";
        Object[] params = { user.getName(), user.getFirstName(), user.getLastName(), user.getPassword(), new Date()};
        int[] types = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP };
        int row = this.template.update(insertSql, params, types);
        System.out.println(row + " row inserted.");
    }
    public User byId(int id) throws DataAccessException {
        String sql = "select id, name, password, create_date from USERS where id = :id";
        SqlParameterSource namedParameters = new MapSqlParameterSource("id", Integer.valueOf(id));
        return (User)this.jdbcTemplate.queryForObject(sql, namedParameters, userMapper);
    }
    public User byName(final String name) throws DataAccessException{
        String sql = "select id, name, password, create_date from USERS where name = :name";
        SqlParameterSource namedParameters = new MapSqlParameterSource("name", name);
        return (User)this.jdbcTemplate.queryForObject(sql, namedParameters, userMapper);
    }
    public User byLogin(final String name, final String password)  throws DataAccessException{
        String sql = "select id, name, password, create_date from USERS where name = :name and password = :password";
        Map<String, String> paramsMap = new HashMap<String, String>();
        paramsMap.put("name", name);
        paramsMap.put("password", password);
        SqlParameterSource namedParameters = new MapSqlParameterSource(paramsMap);
        return (User)this.jdbcTemplate.queryForObject(sql, namedParameters, userMapper);
    }
    public List<User> queryAll() throws DataAccessException{
        String sql = "select id, name, password, create_date from USERS";
        List<User> users = this.jdbcTemplate.query(sql, userMapper);
        return users;
    }
}
